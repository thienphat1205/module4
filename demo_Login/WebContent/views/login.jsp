<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>Login</title>
<link rel="stylesheet" type="text/css" href="../css/login.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
	<div class="frames-login">
		<div class="login-mid">
			<div class="login-header">
				<i class="fa fa-lock"></i>&ensp; <strong> Login To Intranet
					DASHBOARD</strong>
			</div>
			<div class="login-body">
				<div class="login-logo">
					<img src="../images/logo.png">
				</div>
				<c:if test="${not empty msg}">
						<div class="error">${msg}</div><br>
	
					</c:if>
				<div class="login-form">
					<form method="Post">
						<label for="email"><i class="fa fa-user"></i></label> <input
							class="email" type="text" placeholder="Your Company Email"
							name="ema" required> <br> 
							<label for="psw"><i	class="fa fa-lock"></i></label>
							 <input class="passWord" type="password"
							placeholder="Your Password" name="psw" required> <br>
						<input class="checkbox" type="checkbox" name="remember"
							value="1"> Remember me on this computer <br>
						<button class="signin" type="submit">
							SIGN IN<i class="fa fa-sign-in"></i>
						</button>
						<a class="forgot" style="cursor: pointer; color: #444444;"
							onclick="showForgotPass()">Forgot Your Password ?</a>
					</form>					
				</div>
			</div>
		</div>
	</div>
</body>
</html>