<%@page import="model.User"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<title>Terralogic Inc.</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="/demo_Login/css/css.css">
<link rel="stylesheet" type="text/css"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>

	<div class="row">
		<div class="infomation">
			<div class="row">
				<!-- start information1 -->
				<div class="col-sm-3">
					<div class="infomation1">
						<div class="text-center">
							<div class="profile-img" >
								<img src="${o.images}"
									style="height: 140px; width: 140px; border-radius: 5px;" />
							</div>
							<div class="profile-name"></div>
							<div class="profile-title">Intern</div>
							<div class="profile-wstatus">Working Status: Training</div>
						</div>
						<div class="menu">
							<ul>
								<li><a class="border-right" href="#">Personal
										Information</a></li>
								<li><a href="#">Company Information</a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- complete infomation 1 -->
				<!-- start infomation 2 -->
				<div class="col-sm-9">
					<div class="infomation2">
						<!-- start row 1 "Personanl Info + 2 button" -->
						<div class="row">
							<div class="col-sm-4">
								<div class="person-info">Personal Infomation</div>
							</div>

							<div class="col-sm-8 pull-right text-right">

								<form action="">
									<button class="buttonchangepassword" type="submit">
										<i class="fa fa-key"> Change Password </i>
									</button>
									<button class="buttonchangeavt" type="submit">
										<i class="fa fa-user"> Change Avatar </i>
									</button>
								</form>
							</div>
						</div>
						<!-- complete row 1 "Personanl Info + 2 button" -->
						<!-- start row 2 "profile view" -->
						<div class="row">
							<div class="Prolile-view">
								<!-- ID+Joined -->
								<div class="row value-view">
									<div class="col-sm-6">
										<!-- ID -->
										<div class="row">
											<div class="col-sm-3">ID</div>
											<div class="col-sm-3 myvalue_view">${o.userId}</div>
										</div>
									</div>

								</div>

								<div class="row value-view">
									<div class="col-sm-6">
										<div class="row">
											<div class="col-sm-3">ID Card no</div>
											<div class="col-sm-9 myvalue_view">${o.idCard}</div>
										</div>
									</div>
								</div>
								<!---->


								<!-- -->
								<div class="row value-view">
									<div class="col-sm-6">
										<div class="row">
											<div class="col-sm-3">Full Name</div>
											<div class="col-sm-9 myvalue_view">${o.fullName}</div>
										</div>

									</div>

								</div>
								<!---->
								<!-- -->
								<div class="row value-view">
									<div class="col-sm-6">
										<div class="row">
											<div class="col-sm-3">Date of Birth</div>
											<div class="col-sm-9 myvalue_view">${o.birthday}</div>
										</div>

									</div>
								</div>

								<div class="row value-view">
									<div class="col-sm-6">
										<div class="row">
											<div class="col-sm-3">Email</div>
											<div class="col-sm-9 myvalue_view">${o.email}</div>
										</div>

									</div>

								</div>

							</div>
							<!-- complete row 2 "prolife view" -->
						</div>
					</div>
					<!-- complete infomation 2 -->


				</div>
			</div>
		</div>
	</div>

</body>
</html>