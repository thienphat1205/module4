package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.User;
import model.UserRepository;

@WebServlet("/page/login.html")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	UserRepository repository = new UserRepository();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("/views/login.jsp").forward(req, resp);
	}

	private static void savedCookie(HttpServletRequest request, HttpServletResponse response) {
		for (Cookie cookie : request.getCookies()) {
			if (cookie.getName().equals("JSESSIONID")) {
				cookie.setMaxAge(30 * 24 * 3600);
				cookie.setPath(request.getContextPath());
				response.addCookie(cookie);
			}
		}
	}

	private static void savedSession(User obj, HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = request.getSession();
		session.setAttribute("user", obj);

		if (request.getParameter("remember") != null) {
			savedCookie(request, response); 
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		try {
			User ret = repository.logIn(req.getParameter("ema"), req.getParameter("psw"));
			if (ret != null) {
				savedSession(ret, req, resp);
				String Email =req.getParameter("ema");
				String Password =req.getParameter("psw");
				req.setAttribute("o", repository.logIn(Email, Password));
				resp.sendRedirect(req.getContextPath() + "/page/user/myprofile.html");
			} else {
				req.setAttribute("msg", "<i class=\"fa fa-exclamation-triangle\"></i> Invalid Username/Password");
				doGet(req, resp);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
