package model;

import java.sql.Date;

public class User {
	private int userId;
	private String fullName;
	private Date birthday;
	private String email;
	private int idCard;
	private String password;
	private int tel;
	private String images;


	
	public User(int userId, String fullName, Date birthday, String email, int idCard, String password, int tel,
			String images) {
		super();
		this.userId = userId;
		this.fullName = fullName;
		this.birthday = birthday;
		this.email = email;
		this.idCard = idCard;
		this.password = password;
		this.tel = tel;
		this.images = images;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getIdCard() {
		return idCard;
	}

	public void setIdCard(int idCard) {
		this.idCard = idCard;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getTel() {
		return tel;
	}

	public void setTel(int tel) {
		this.tel = tel;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

}
