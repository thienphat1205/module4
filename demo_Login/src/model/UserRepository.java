package model;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class UserRepository extends Repository {
	// Login:
	public User logIn(String Email, String Passord) throws SQLException {
		try {
			open();
			pstmt = connection.prepareStatement("SELECT * FROM User WHERE Email = ? AND Password = ?");
			pstmt.setString(1, Email);
			pstmt.setString(2, Passord);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				return new User(
						rs.getInt("UserId"), 
						rs.getString("Fullname"),
						rs.getDate("Birthday"),
						rs.getString("Email"),
						rs.getInt("IDcard"), 
						rs.getString("Password"),
						rs.getInt("Tel"),
						rs.getString("Images"));
					 
			}
			return null;
		} finally {
			close();
		}
	}
	
}
